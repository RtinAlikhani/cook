package com.example.cook;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.android.volley.Response;
import com.example.cook.adapter.BannerAdapter;
import com.example.cook.adapter.CategoryAdapter;
import com.example.cook.adapter.RecipeAdapter;
import com.example.cook.data.Banner;
import com.example.cook.data.Category;
import com.example.cook.data.Recipe;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    ApiService apiService;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        apiService = new ApiService(MainActivity.this);

        setupViews();
    }

    private void setupViews() {
        getBanners();
        getCategries();
        getRecipes();
    }

    private void getRecipes() {
        apiService.getRecipes(new Response.Listener<List<Recipe>>() {
            @Override
            public void onResponse(List<Recipe> recipes) {
                RecyclerView recipRv = findViewById(R.id.rv_main_recipes);
                recipRv.setLayoutManager(new StaggeredGridLayoutManager(2, RecyclerView.VERTICAL));
                recipRv.setAdapter(new RecipeAdapter(recipes));
            }
        });
    }

    private void getCategries() {
        apiService.getCategories(new Response.Listener<List<Category>>() {
            @Override
            public void onResponse(List<Category> categories) {
                RecyclerView categoryRv = findViewById(R.id.rv_main_categories);
                categoryRv.setLayoutManager(new LinearLayoutManager(MainActivity.this, RecyclerView.HORIZONTAL, false));
                categoryRv.setAdapter(new CategoryAdapter(categories));
            }
        });

    }

    private void getBanners() {
        apiService.getBanners(new Response.Listener<List<Banner>>() {
            @Override
            public void onResponse(List<Banner> banners) {
                RecyclerView bannerRv = findViewById(R.id.rv_main_sliders);
                bannerRv.setLayoutManager(new LinearLayoutManager(MainActivity.this, RecyclerView.HORIZONTAL, false));
                bannerRv.setAdapter(new BannerAdapter(banners));

                SnapHelper snapHelper = new PagerSnapHelper();
                snapHelper.attachToRecyclerView(bannerRv);
            }
        });
    }
}
