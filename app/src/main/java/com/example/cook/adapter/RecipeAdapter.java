package com.example.cook.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.cook.R;
import com.example.cook.data.Recipe;
import com.squareup.picasso.Picasso;

import java.util.List;

public class RecipeAdapter extends RecyclerView.Adapter<RecipeAdapter.RecipeViewHolder> {

    private List<Recipe> recipes;

    public RecipeAdapter(List<Recipe> recipes) {

        this.recipes = recipes;
    }

    @NonNull
    @Override
    public RecipeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RecipeViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recipe, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecipeViewHolder holder, int position) {
        holder.bindRecipe(recipes.get(position));
    }

    @Override
    public int getItemCount() {
        return recipes.size();
    }

    public class RecipeViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        TextView author;
        TextView desc;
        ImageView iconRecipe;
        RatingBar ratingBar;

        public RecipeViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.tv_recipe_title);
            author = itemView.findViewById(R.id.tv_recipe_author);
            desc = itemView.findViewById(R.id.tv_recipe_desc);
            iconRecipe = itemView.findViewById(R.id.iv_recipe);
            ratingBar = itemView.findViewById(R.id.rb_recipe);

        }

        public void bindRecipe(Recipe recipe) {
            String url=recipe.getImg().replace("http","https");
            Log.e("img", "bindBanner: " + url);
            Picasso.get().load(recipe.getImg()).into(iconRecipe);
            title.setText(recipe.getTitle());
            author.setText("By " + recipe.getDesc());
            desc.setText(recipe.getDesc());
            ratingBar.setRating((float) recipe.getRate());

        }
    }
}
